In the last decade, path discovery has been extensively covered in literature; in its
simplest form, it generally works by sending probes that expire along the
path from a host to a destination. It is also known that network administrators
often configure their routers to limit the amount of ICMP replies sent, a common
practice typically referred to as ICMP rate limitation. In this paper
we attempt to characterize the responsiveness of routers to expiring ICMP echo-request
packets. Our contribution is twofold: we first show that for the vast majority
of routers the delay of replies is not affected by the probing rate and then we
provide a detailed analysis of how routers are most commonly configured.
In our dataset of over 800 routers spanning the first 5 hops, roughly 45% of
routers were rate-limited, 25% responded to nearly all probes and only 5% were unresponsive.
Our study will mainly focus on that 45% that showed a regular bursty behaviour.
